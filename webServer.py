#SPAHIU ALBI
#MATRICOLA: 880410
#TRACCIA 2

from http.server import SimpleHTTPRequestHandler
import sys,signal
import socketserver
import smtplib
import cgi

#numero di porta
port = 8080

#host
host = 'localhost'

#mittente
sender = 'progettoretitraccia2@gmail.com'

#password mittente
password = 'serverPython'

#funzione per mandare l'email
def send_email():
    
    #legge l'email
    receiver = open("txt/email.txt","r").read()
    
    message = "Benvenuto nella newsletter di TUTTO SCACCHI!"
        
    try:
           serverEmail = smtplib.SMTP('smtp.gmail.com', 587)
           
           #connessione con il server
           serverEmail.ehlo()
           
           #canale criptato
           serverEmail.starttls()

           #login al server
           serverEmail.login(sender, password)
           
           #spedisce email
           serverEmail.sendmail(sender, receiver, message)
           
           #chiude la connessione
           serverEmail.close()
           
           print("Send email")
           
    except Exception:
           print ("Error: unable to send email")
    


#MyServer è la classa usata per gestire le richieste HTTP
#MyServer estende SimpleHTTPRequestHandler
class MyServer(SimpleHTTPRequestHandler):

    print('Server is running...')

    #funzione GET
    def do_GET(self):
        
        SimpleHTTPRequestHandler.do_GET(self)

    #funzione POST        
    def do_POST(self):
        
        #use FieldStorage class to get at submitted from data
        form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['Content-Type'],
                         })
        
        SimpleHTTPRequestHandler.do_GET(self)
        
        #scrive l'ultima mail su file
        with open("txt/email.txt", "w") as file:
                for key in form.keys(): 
                    file.write(str(form.getvalue(str(key))))
        
        #salva l'email della newsletter
        with open("txt/register.txt", "a") as file:
                for key in form.keys(): 
                    file.write(str(form.getvalue(str(key))) + ',')
                    
        #richiama la funzione per spedire l'email         
        send_email()
        

#ThreadingTCPServer necessaria per gestire più richieste contemporaneamente
server = socketserver.ThreadingTCPServer((host, port), MyServer)

#Thread autogestiti
server.daemon_threads = True

# Indichiamo al server di riutilizzare la stessa porta
server.allow_reuse_address = True

#funzione per uscire dal processo
def signal_handler(signal, frame):
    print( 'Exiting http server (Ctrl+C pressed)')
    try:
      if( server ):
        server.server_close()
    finally:
      sys.exit(0)

#Interrompe l'esecuzione premendo Ctrl-C
signal.signal(signal.SIGINT, signal_handler)

#loop che gestisce le richieste finchè il programma non termina
try:
    
    while True:
        server.serve_forever()

except KeyboardInterrupt:  
    pass

server.server_close()
        

        
