# PROGETTO RETI - Python Web Server #

Nome: Albi   
Cognome: Spahiu   
Matricola: 880410   
Traccia: 2   

Dopo aver letto le specifiche della traccia, ho deciso di realizzare un Web Server Python per un testata giornalistica sugli scacchi, chiamata "Tutto Scacchi". Il sito presenta tutti i requisiti richiesti dalla traccia:

* supporta più connessioni in contemporanea
* una index.html contenente immagini e titoli degli articoli
* i titoli degli articoli contengono link ad altre pagine web con il testo dell'articolo
mostrato per intero
* la index, cosi come le altre pagine, presentano un link per il download di un file pdf

In aggiunta:

* è presente la sezione "newsletter" dove si potrà inserire l'email per essere aggiornati su tutte le ultime novità

### WEB SERVER PYTHON ###

Per avviare il web server è necessario eseguire il file webServer.py e accedere al sito da localhost (127.0.0.1), posta 8080.
Il codice, rigorsamente commentato, contiene una funzione send_mail() che gestisce l'iscrizione alla newsletter mandando una email di conferma ai nuovi iscritti. La funzione fa uso del modulo smtplib per gestire la sessione SMTP. La classe MyServer, che gestisce le richieste HTTP, è stata implementata estendo SimpleHTTPRequestHandler per poter sovrascrivere il metodo POST, che ora gestisce l'iscrizione alla newsletter.
Viene poi usato ThreadingTCPServer per avere un server in grado di gestire più richieste contemporaneamente. È stata definita la funzione signal_handler() che permette di uscire dal processo premendo Ctrl-C.

### HTML e CSS ###

Questa è stata la parte più ostica del progetto, avendo solo un po' di conoscenze pregresse di HTML e nessuna di CSS. Per quanto riguarda la parte di HTML, ho deciso di mettere una barra superiore dalla quale fosse possibile: tornare alla home, scaricare il calendario e iscriversi alla newsletter. Ho poi aggiunto un "header" con il titolo del sito presente in tutte le pagine. Per CSS ho scaricato un template online sul quale ho effettuato le dovute modifiche per dare al sito lo stile voluto.

